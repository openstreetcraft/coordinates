# coordinates

Java library for converting coordinates from a [geographic coordinate system](https://en.wikipedia.org/wiki/Geographic_coordinate_system) into a [projected coordinate system](https://en.wikipedia.org/wiki/Projected_coordinate_system) and vice-versa.

## Installation

See [GitLab Package Registry](https://gitlab.com/openstreetcraft/coordinates/-/packages/22179106).

## Usage

Create `Projection` instance for a given map:

```
Projection projection = new ProjectionBuilder("earth").build();
```

Convert Geodetic datum into Minecraft block coordinate:

```
SphericalLocation datum = SphericalLocation.of(30.0, 60.0);
ProjectedLocation projected = projection.transform(datum);
BlockLocation location = BlockLocation.of(projected);
Block2D block = Block2D.of(location);

assertEquals("ProjectedLocation(x=3339584.723798207, y=6679169.447596414)", projected.toString());
assertEquals("BlockLocation(x=3339584.723798207, z=-6679169.447596414)", location.toString());
assertEquals("Block2D(x=3339584, z=-6679169)", block.toString());
```

Convert Minecraft block coordinate into Geodetic datum:

```
Block2D block = Block2D.of(3339584, -6679169);
BlockLocation location = BlockLocation.of(block);
ProjectedLocation projected = ProjectedLocation.of(location);
SphericalLocation datum = projection.transform(projected);

assertEquals("BlockLocation(x=3339584.0, z=-6679169.0)", location.toString());
assertEquals("ProjectedLocation(x=3339584.0, y=6679169.0)", projected.toString());
assertEquals("SphericalLocation(longitude=29.999993498010078, latitude=59.99999597917299)", datum.toString());
```

## License

[GNU LGPL Version 3](https://www.gnu.org/licenses/lgpl-3.0.html)
