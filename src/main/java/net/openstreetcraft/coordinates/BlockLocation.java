package net.openstreetcraft.coordinates;

import javax.naming.OperationNotSupportedException;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.openstreetcraft.projection.ProjectedLocation;

/**
 * Minecraft world coordinates are based on a 2-dimensional grid with the the x-axis indicating the
 * players distance east (positive) or west (negative) of the origin point and the z-axis indicating
 * the players distance south (positive) or north (negative) of the origin point. The unit length of
 * the axes equals the sides of one block is 1x1 meter. The grid coordinates of a block are actually
 * the coordinates of the point at the lower north-west corner of the block as integer values by
 * rounding down the double coordinates inside the block.
 */
@Data
@Builder
@Setter(AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BlockLocation {
  double x;
  double z;

  /**
   * @deprecated use BlockLocation.of(double,double) or Block2D.of(int,int)
   */
  @Deprecated
  public static BlockLocation of(int x, int z) throws OperationNotSupportedException {
    throw new OperationNotSupportedException();
  }
  
  public static BlockLocation of(double x, double z) {
    return new BlockLocation(x, z);
  }
  
  public static BlockLocation of(Block2D block) {
    return new BlockLocation(block.x, block.z);
  }

  public static BlockLocation of(ProjectedLocation projected) {
    return new BlockLocation(projected.getX(), -projected.getY());
  }

  public boolean inside(BoundingBox<BlockLocation> bounds) {
    BlockLocation nw = bounds.getNw();
    BlockLocation so = bounds.getSo();
    return nw.x <= x && x <= so.x
        && nw.z <= z && z <= so.z;
  }

  public static BlockLocation center(BoundingBox<BlockLocation> bounds) {
    BlockLocation nw = bounds.getNw();
    BlockLocation so = bounds.getSo();
    return BlockLocation.of(avg(nw.x, so.x), avg(nw.z, so.z));
  }

  private static double avg(double a, double b) {
    return (a + b) / 2;
  }
}
