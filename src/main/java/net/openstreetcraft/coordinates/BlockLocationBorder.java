package net.openstreetcraft.coordinates;

import java.util.function.Function;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class BlockLocationBorder implements Function<BoundingBox<BlockLocation>, BoundingBox<BlockLocation>> {

  final double border;
  
  @Override
  public BoundingBox<BlockLocation> apply(BoundingBox<BlockLocation> src) {
    return BoundingBox.of(
        BlockLocation.of(src.nw.x - border, src.nw.z - border),
        BlockLocation.of(src.so.x + border, src.so.z + border));
  }
  
}
