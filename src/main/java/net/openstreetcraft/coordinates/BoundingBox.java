package net.openstreetcraft.coordinates;

import java.util.function.Function;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Setter(AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoundingBox<T> {
  T nw;
  T so;

  public static <T> BoundingBox<T> of(T nw, T so) {
    return new BoundingBox<T>(nw, so);
  }
  
  public <R> BoundingBox<R> forEach(Function<T, R> mapper) {
    return BoundingBox.of(mapper.apply(nw), mapper.apply(so));
  }

  public <R> BoundingBox<R> map(Function<BoundingBox<T>, BoundingBox<R>> mapper) {
    return mapper.apply(this);
  }

  public <R> R collect(Function<BoundingBox<T>, R> mapper) {
    return mapper.apply(this);
  }
}
