package net.openstreetcraft.coordinates;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Setter(AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Block2D {
  int x;
  int z;
  
  public static Block2D of(int x, int z) {
    return new Block2D(x, z);
  }

  public static Block2D of(BlockLocation location) {
    return new Block2D((int) location.x, (int) location.z);
  }
  
  public boolean contains(BlockLocation block) {
    return x <= block.x && block.x <= x + 1
        && z <= block.z && block.z <= z + 1;
  }
  
  public BoundingBox<BlockLocation> bounds() {
    return BoundingBox.of(new BlockLocation(x, z), new BlockLocation(x + 1, z + 1));
  }
  
}
