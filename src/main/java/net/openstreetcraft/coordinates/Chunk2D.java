package net.openstreetcraft.coordinates;

import java.util.function.Consumer;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * A chunk in Minecraft is a procedurally generated 16 x 16 segment of the world that extends all
 * the way down to the bedrock up to a height of 256 blocks.
 */
@Data
@Setter(AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Chunk2D {
  int x;
  int z;
  
  public static Chunk2D of(int x, int z) {
    return new Chunk2D(x, z);
  }
  
  public static Chunk2D of(ChunkLocation location) {
    return new Chunk2D((int) location.x, (int) location.z);
  }
  
  public static Chunk2D of(Block2D block) {
    return new Chunk2D(chunk(block.x), chunk(block.z));
  }
  
  public void forEach(Consumer<Block2D> consumer) {
    Block2D v = new Block2D();
    v.x = block(x);
    for (int dx = 0; dx < 16; dx++, v.x++) {
      v.z = block(z);
      for (int dz = 0; dz < 16; dz++, v.z++) {
        consumer.accept(v);
      }
    }
  }
  
  public boolean contains(Block2D block) {
    int bx = block(x);
    int bz = block(z);
    return bx <= block.x && block.x <= bx + 15
        && bz <= block.z && block.z <= bz + 15;
  }
  
  public BoundingBox<Block2D> bounds() {
    int bx = block(x);
    int bz = block(z);
    return BoundingBox.of(new Block2D(bx, bz), new Block2D(bx + 15, bz + 15));
  }
  
  private static int chunk(int block) {
    return block >> 4;
  }

  private static int block(int chunk) {
    return chunk << 4;
  }
}
