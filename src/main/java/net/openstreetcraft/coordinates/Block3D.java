package net.openstreetcraft.coordinates;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@EqualsAndHashCode(callSuper = true)
@Setter(AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Block3D extends Block2D {
  int y;
  
  protected Block3D(int x, int y, int z) {
    super(x, z);
    this.y = y;
  }

  public static Block3D of(int x, int y, int z) {
    return new Block3D(x, y, z);
  }

}
