package net.openstreetcraft.coordinates;

import javax.naming.OperationNotSupportedException;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * This is the NW chunk coordinate of a Minecraft chunk. Chunk coordinates are block coordinates
 * divided by 16.
 * 
 * @see Block2D
 * @see Chunk2D
 */
@Data
@Builder
@Setter(AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ChunkLocation {
  double x;
  double z;

  /**
   * @deprecated use ChunkLocation.of(double,double) or Chunk2D.of(int,int)
   */
  @Deprecated
  public static ChunkLocation of(int x, int z) throws OperationNotSupportedException {
    throw new OperationNotSupportedException();
  }
  
  /**
   * @deprecated use ChunkLocation.of(Chunk2D)
   */
  @Deprecated
  public static ChunkLocation of(double x, double z) {
    return new ChunkLocation(x, z);
  }
  
  public static ChunkLocation of(Chunk2D chunk) {
    return new ChunkLocation(chunk.x, chunk.z);
  }
  
  public BoundingBox<BlockLocation> bounds() {
    double bx = block(x);
    double bz = block(z);
    return BoundingBox.of(BlockLocation.of(bx, bz), BlockLocation.of(bx + 16, bz + 16));
  }

  private static double block(double chunk) {
    return chunk * 16;
  }

}
