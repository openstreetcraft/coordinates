package net.openstreetcraft.coordinates;

import java.util.Comparator;

public class BlockLocationComparator implements Comparator<BlockLocation> {

  @Override
  public int compare(BlockLocation lhs, BlockLocation rhs) {
    int cmp = Double.compare(lhs.x, rhs.x);
    return (cmp != 0) ? cmp : Double.compare(lhs.z, rhs.z);
  }

}
