package net.openstreetcraft.coordinates;

import java.util.function.Function;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class BlockLocationGrid implements Function<BoundingBox<BlockLocation>, BoundingBox<BlockLocation>> {

  final double grid;

  /**
   * Align the corners of the bounding box so that they are on grid coordinates. The original
   * bounding box is completely contained within the result.
   */
  @Override
  public BoundingBox<BlockLocation> apply(BoundingBox<BlockLocation> src) {
    return BoundingBox.of(
        BlockLocation.of(left(src.nw.x), left(src.nw.z)),
        BlockLocation.of(right(src.so.x), right(src.so.z)));
  }

  private double left(double x) {
    return grid * (int) (x / grid);
  }
  
  private double right(double x) {
    return left(x) + grid;
  }

}
