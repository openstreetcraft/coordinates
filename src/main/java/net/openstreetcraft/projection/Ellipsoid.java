// Copyright (C) 2017 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.projection;

public class Ellipsoid {

  public static final Ellipsoid GRS80 = new Ellipsoid(6378137, 6356752.31414);
  public static final Ellipsoid WGS84 = new Ellipsoid(6378137, 6356752.31424518);
  public static final Ellipsoid WEBMERC = new Ellipsoid(6378137, 6378137);
  public static final Ellipsoid MARS = new Ellipsoid(3396200, 3376200);
  public static final Ellipsoid MOON = new Ellipsoid(1738100, 1736000);

  private final double equatorialRadius;
  private final double polarRadius;

  private Ellipsoid(double equatorialRadius, double polarRadius) {
    this.equatorialRadius = equatorialRadius;
    this.polarRadius = polarRadius;
  }

  public double getEquatorialRadius() {
    return equatorialRadius;
  }

  public double getPolarRadius() {
    return polarRadius;
  }

  public double getInverseFlattening() {
    return (equatorialRadius - polarRadius) / equatorialRadius;
  }
}
