package net.openstreetcraft.projection;

import java.util.Locale;

import lombok.Builder;

@Builder
public class Wktext {
  String proj;
  double a;
  double b;
  double lon0;
  double lat0;
  
  @Override
  public String toString() {
    return String.format(Locale.US,
        "+wktext +proj=%s +a=%f +b=%f +lon_0=%f +lat_0=%f",
        proj, a, b, lon0, lat0);
  }
  
  public static class WktextBuilder {
    public WktextBuilder ellipsoid(Ellipsoid ellipsoid) {
      return a(ellipsoid.getEquatorialRadius())
          .b(ellipsoid.getPolarRadius());
    }

    public WktextBuilder origin(SphericalLocation origin) {
      return lon0(origin.getLongitude())
          .lat0(origin.getLatitude());
    }
  }
}
