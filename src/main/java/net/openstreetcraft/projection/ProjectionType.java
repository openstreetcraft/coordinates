package net.openstreetcraft.projection;

import java.util.Locale;

public enum ProjectionType {
  /**
   * This simple cylindrical projection converts the globe into a Cartesian grid. Each rectangular
   * grid cell has the same size, shape, and area. All the graticular intersections are 90°. The
   * central parallel may be any line, but the traditional Plate Carrée projection uses the equator.
   * When the equator is used, the grid cells are perfect squares, but if any other parallel is
   * used, the grids become rectangular. In this projection, the poles are represented as straight
   * lines across the top and bottom of the grid.
   * <p>
   * Also known as equirectangular, simple cylindrical, rectangular, or Plate Carrée (if the
   * standard parallel is the equator).
   * <p>
   * Its official EPSG identifier is ESRI:53001.
   */
  EQC,

  /**
   * The Web Mercator projection is a cylindrical map projection. This is a variant of the regular
   * Mercator projection, except that the computation is done on a sphere, using the semi-major axis
   * of the ellipsoid.
   * <p>
   * This projection is widely used and is the de-facto standard for Web mapping applications.
   * <p>
   * Its official EPSG identifier is EPSG:3857.
   */
  MERC,
  WEBMERC;

  @Override
  public String toString() {
    return name().toLowerCase(Locale.US);
  }
}
