package net.openstreetcraft.projection;

import org.locationtech.proj4j.CRSFactory;
import org.locationtech.proj4j.CoordinateReferenceSystem;
import org.locationtech.proj4j.CoordinateTransform;
import org.locationtech.proj4j.CoordinateTransformFactory;
import org.locationtech.proj4j.Proj4jException;
import org.locationtech.proj4j.ProjCoordinate;

/**
 * Base class for all projections transforming from a geographic coordinate system into a projected
 * coordinate system and vica-verse.
 * <p>
 * A geographic coordinate system uses a spherical surface to define locations on an idealized
 * Earth. The idealized earth is a perfect ellipsoid with an equator radius and a polar radius. A
 * point on the geographic coordinate system is referenced by its longitude and latitude values in
 * degrees (see {@link SphericalLocation}).
 * <p>
 * A projected coordinate system is defined on a flat, two-dimensional surface. In a projected
 * coordinate system, locations are identified by x,y coordinates on a grid (see {@link
 * ProjectedLocation}). The parameters of the projected coordinate system are defined by each
 * subclass in "well known text" (wktext) format.
 */
class SphericalProjection extends Projection {

  private final CoordinateTransform forward;
  private final CoordinateTransform reverse;

  public SphericalProjection(String src, String dst) {
    this(spatialReference(src), spatialReference(dst));
  }

  private SphericalProjection(CoordinateReferenceSystem src, CoordinateReferenceSystem dst) {
    CoordinateTransformFactory factory = new CoordinateTransformFactory();
    forward = factory.createTransform(src, dst);
    reverse = factory.createTransform(dst, src);
  }

  private static CoordinateReferenceSystem spatialReference(String wktext) {
    CRSFactory factory = new CRSFactory();
    return factory.createFromParameters(null, wktext);
  }

  @Override
  public ProjectedLocation transform(SphericalLocation location) {
    double[] result = transform(forward, location.getLongitude(), location.getLatitude());
    return new ProjectedLocation(result[0], result [1]);
  }

  @Override
  public SphericalLocation transform(ProjectedLocation location) {
    return SphericalLocation.of(transform(reverse, location.getX(), location.getY()));
  }

  private static double[] transform(CoordinateTransform transformation, double x, double y) {
    try {
      ProjCoordinate result = new ProjCoordinate();
      transformation.transform(new ProjCoordinate(x, y), result);
      return new double[] { result.x, result.y };
    } catch (Proj4jException exception) {
      return new double[] { Double.NaN, Double.NaN };
    }
  }

}
