package net.openstreetcraft.projection;

import java.util.Comparator;

public class SphericalLocationComparator implements Comparator<SphericalLocation> {
  
  @Override
  public int compare(SphericalLocation lhs, SphericalLocation rhs) {
    int cmp = Double.compare(lhs.longitude, rhs.longitude);
    return (cmp != 0) ? cmp : Double.compare(lhs.latitude, rhs.latitude);
  }

}
