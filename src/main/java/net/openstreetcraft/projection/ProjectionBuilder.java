package net.openstreetcraft.projection;

import java.util.Locale;

import net.openstreetcraft.projection.Wktext.WktextBuilder;

public class ProjectionBuilder {
  
  private SphericalLocation origin = SphericalLocation.of(0, 0);
  private final String map;

  public ProjectionBuilder(String map) {
    this.map = map.toLowerCase(Locale.US);
  }

  public ProjectionBuilder origin(SphericalLocation origin) {
    this.origin = origin;
    return this;
  }

  public Projection build() {
    WktextBuilder src = Wktext.builder().proj("longlat").ellipsoid(Ellipsoid.WGS84);
    WktextBuilder dst = Wktext.builder().origin(origin);
    
    switch (map) {
      case "earth":
        dst.proj("eqc").ellipsoid(Ellipsoid.WGS84);
        break;

      case "webmerc":
        dst.proj("merc").ellipsoid(Ellipsoid.WEBMERC);
        break;
    }
    
    return new SphericalProjection(src.build().toString(), dst.build().toString());
  }

}
