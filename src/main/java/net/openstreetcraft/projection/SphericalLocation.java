package net.openstreetcraft.projection;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.openstreetcraft.coordinates.BoundingBox;

/**
 * This geographic coordinate system uses latitude in the range −90° ≤ φ ≤ 90° and rotated north
 * from the equator plane, and longitude in the range −180° ≤ λ ≤ 180° from west to east.
 */
@Data
@Builder
@Setter(AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SphericalLocation {

  public static SphericalLocation INVALID = SphericalLocation.of(Double.NaN, Double.NaN);
  
  double longitude;
  double latitude;
  
  public static SphericalLocation of(double[] array) {
    return of(array[0], array[1]);
  }

  public static SphericalLocation of(double longitude, double latitude) {
    return new SphericalLocation(longitude, latitude);
  }
  
  public double[] toArray() {
    return new double[] {longitude, latitude};
  }
  
  public boolean inside(BoundingBox<SphericalLocation> bounds) {
    SphericalLocation nw = bounds.getNw();
    SphericalLocation so = bounds.getSo();
    return longitude >= nw.longitude
        && latitude >= nw.latitude
        && longitude <= so.longitude
        && latitude <= so.latitude;
  }

  public static SphericalLocation center(BoundingBox<SphericalLocation> bounds) {
    SphericalLocation nw = bounds.getNw();
    SphericalLocation so = bounds.getSo();
    return SphericalLocation.of(
        avg(nw.longitude, so.longitude),
        avg(nw.latitude, so.latitude));
  }

  private static double avg(double a, double b) {
    return (a + b) / 2;
  }
}
