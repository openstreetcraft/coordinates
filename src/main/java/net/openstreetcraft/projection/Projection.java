package net.openstreetcraft.projection;

import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class Projection {

  public ProjectedLocation[] transform(SphericalLocation[] locations) {
    ProjectedLocation[] result = new ProjectedLocation[locations.length];
    return (ProjectedLocation[]) Stream.of(locations)
        .parallel()
        .map(this::transform)
        .collect(Collectors.toCollection(ArrayList::new))
        .toArray(result);
  }

  public SphericalLocation[] transform(ProjectedLocation[] locations) {
    SphericalLocation[] result = new SphericalLocation[locations.length];
    return (SphericalLocation[]) Stream.of(locations)
        .parallel()
        .map(this::transform)
        .collect(Collectors.toCollection(ArrayList::new))
        .toArray(result);
  }

  public abstract ProjectedLocation transform(SphericalLocation location);

  public abstract SphericalLocation transform(ProjectedLocation location);

}
