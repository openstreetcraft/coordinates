package net.openstreetcraft.projection;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.openstreetcraft.coordinates.BlockLocation;

@Data
@Builder
@Setter(AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProjectedLocation {
  
  public static ProjectedLocation INVALID = ProjectedLocation.of(Double.NaN, Double.NaN);
  
  private double x;
  private double y;
  
  public static ProjectedLocation of(double x, double y) {
    return new ProjectedLocation(x, y);
  }

  public static ProjectedLocation of(BlockLocation block) {
    return new ProjectedLocation(block.getX(), -block.getZ());
  }
}
