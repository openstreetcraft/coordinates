package net.openstreetcraft.projection;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SphericalLocationMatcher extends BaseMatcher<SphericalLocation> {

  final SphericalLocation expected;
  final double precision;
  
  @Override
  public boolean matches(Object item) {
    SphericalLocation actual = (SphericalLocation) item;
    assertEquals(expected.latitude, actual.latitude, precision);
    assertEquals(expected.longitude, actual.longitude, precision);
    return true;
  }

  @Override
  public void describeTo(Description description) {
    // TODO Auto-generated method stub
    
  }

}
