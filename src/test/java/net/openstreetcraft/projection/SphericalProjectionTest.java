package net.openstreetcraft.projection;

import java.util.ArrayList;
import java.util.stream.Stream;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import net.openstreetcraft.coordinates.Block2D;
import net.openstreetcraft.coordinates.BlockLocation;

/**
 * @see https://epsg.io/transform
 */
class SphericalProjectionTest {

  private static Stream<Arguments> provideEqcCoordinates() {
    return Stream.of(
        Arguments.of(ProjectedLocation.INVALID, SphericalLocation.INVALID),
        Arguments.of(new ProjectedLocation(0.0, 0.0), SphericalLocation.of(0.0, 0.0)),
        Arguments.of(new ProjectedLocation(0.0, 10018754.0), SphericalLocation.of(0.0, 90.0)),
        Arguments.of(new ProjectedLocation(0.0, -10018754.0), SphericalLocation.of(0.0, -90.0)),
        Arguments.of(new ProjectedLocation(20037508, 0.0), SphericalLocation.of(180.0, 0.0)),
        Arguments.of(new ProjectedLocation(-20037508.0, 0.0), SphericalLocation.of(-180.0, 0.0)),
        Arguments.of(new ProjectedLocation(3339585.0, 6679169.0), SphericalLocation.of(30.0, 60.0)),
        Arguments.of(new ProjectedLocation(-16697924.0, 10018754.0), SphericalLocation.of(-150.0, 90.0)),
        Arguments.of(new ProjectedLocation(-6679169.0, -3339585.0), SphericalLocation.of(-60.0, -30.0)),
        Arguments.of(new ProjectedLocation(13358339.0, -3339585.0), SphericalLocation.of(120.0, -30.0)));
  }

  @ParameterizedTest
  @MethodSource("provideEqcCoordinates")
  public void transformEquidistantCylindrical(ProjectedLocation minecraft, SphericalLocation spherical) {
    Projection projection = new ProjectionBuilder("earth").build();

    assertEquals(minecraft, projection.transform(spherical));
    assertEquals(spherical, projection.transform(minecraft));
  }
  
  @Test
  public void map() {
    Projection projection = new ProjectionBuilder("earth").build();

    ArrayList<ProjectedLocation> locations = new ArrayList<>();
    ArrayList<SphericalLocationMatcher> matchers = new ArrayList<>();
    
    provideEqcCoordinates().map(Arguments::get).forEach(args -> {
      locations.add((ProjectedLocation) args[0]);
      matchers.add(new SphericalLocationMatcher((SphericalLocation) args[1], 0.1));
    });

    ProjectedLocation[] array = new ProjectedLocation[locations.size()];
    array = locations.toArray(array);
    
    SphericalLocation[] actual = projection.transform(array);
    
    SphericalLocationMatcher[] expected = new SphericalLocationMatcher[matchers.size()];
    expected = matchers.toArray(expected);
    
    MatcherAssert.assertThat(actual, Matchers.array(expected));
  }

  @SuppressWarnings("unused")
  private static Stream<Arguments> provideWebmercCoordinates() {
    return Stream.of(
        Arguments.of(ProjectedLocation.INVALID, SphericalLocation.INVALID),
        Arguments.of(new ProjectedLocation(0.0, 0.0), SphericalLocation.of(0.0, 0.0)),
        Arguments.of(new ProjectedLocation(0.0, 30240972.0), SphericalLocation.of(0.0, 89.0)),
        Arguments.of(new ProjectedLocation(0.0, -15538711.0), SphericalLocation.of(0.0, -80.0)),
        Arguments.of(new ProjectedLocation(20037508.3428, 0.0), SphericalLocation.of(180.0, 0.0)),
        Arguments.of(new ProjectedLocation(-20037508.3428, 0.0), SphericalLocation.of(-180.0, 0.0)),
        Arguments.of(new ProjectedLocation(3339585.0, 8399737.0), SphericalLocation.of(30.0, 60.0)),
        Arguments.of(new ProjectedLocation(-16697924.0, 25819498.0), SphericalLocation.of(-150.0, 88.0)),
        Arguments.of(new ProjectedLocation(-6679169.0, -3503550.0), SphericalLocation.of(-60, -30.0)),
        Arguments.of(new ProjectedLocation(13358339.0, -3503550.0), SphericalLocation.of(120.0, -30.0)));
  }

  @ParameterizedTest
  @MethodSource("provideWebmercCoordinates")
  public void transformWebMercator(ProjectedLocation minecraft, SphericalLocation spherical) {
    Projection projection = new ProjectionBuilder("webmerc").build();

    assertEquals(minecraft, projection.transform(spherical));
    assertEquals(spherical, projection.transform(minecraft));
  }
  
  @SuppressWarnings("unused")
  private static Stream<String> originSupporting() {
    return Stream.of("earth");
  }

  @Disabled
  @ParameterizedTest
  @MethodSource("originSupporting")
  public void supportsOrigin(String map) {
    SphericalLocation origin = SphericalLocation.of(30, 60);
    Projection projection = new ProjectionBuilder(map).origin(origin).build();

    SphericalLocation spherical = origin;
    ProjectedLocation minecraft = new ProjectedLocation(0.0, 0.0);
  
    assertEquals(minecraft, projection.transform(spherical));
    assertEquals(spherical, projection.transform(minecraft));
  }
  
  @Test
  public void transformSphericalLocationIntoBlock2D() {
    Projection projection = new ProjectionBuilder("earth").build();
    SphericalLocation datum = SphericalLocation.of(30.0, 60.0);
    ProjectedLocation projected = projection.transform(datum);
    BlockLocation location = BlockLocation.of(projected);
    Block2D block = Block2D.of(location);

    Assertions.assertEquals("ProjectedLocation(x=3339584.723798207, y=6679169.447596414)", projected.toString());
    Assertions.assertEquals("BlockLocation(x=3339584.723798207, z=-6679169.447596414)", location.toString());
    Assertions.assertEquals("Block2D(x=3339584, z=-6679169)", block.toString());
  }

  @Test
  public void transformBlock2DIntoSphericalLocation() {
    Projection projection = new ProjectionBuilder("earth").build();
    Block2D block = Block2D.of(3339584, -6679169);
    BlockLocation location = BlockLocation.of(block);
    ProjectedLocation projected = ProjectedLocation.of(location);
    SphericalLocation datum = projection.transform(projected);

    Assertions.assertEquals("BlockLocation(x=3339584.0, z=-6679169.0)", location.toString());
    Assertions.assertEquals("ProjectedLocation(x=3339584.0, y=6679169.0)", projected.toString());
    Assertions.assertEquals("SphericalLocation(longitude=29.999993498010078, latitude=59.99999597917299)", datum.toString());
  }

  private static void assertEquals(SphericalLocation expected, SphericalLocation actual) {
    MatcherAssert.assertThat(actual, new SphericalLocationMatcher(expected, 0.00001));
  }

  private static void assertEquals(ProjectedLocation expected, ProjectedLocation actual) {
    MatcherAssert.assertThat(actual, new ProjectedLocationMatcher(expected, 1.0));
  }
}
