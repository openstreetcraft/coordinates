package net.openstreetcraft.projection;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.openstreetcraft.coordinates.BoundingBox;

class SphericalLocationTest {

  ObjectMapper mapper = new ObjectMapper();

  @Test
  void deserialize() throws JacksonException {
    String json = "{\"longitude\":1,\"latitude\":2}";
    SphericalLocation actual = mapper.readValue(json, SphericalLocation.class);
    assertEquals(1.0, actual.getLongitude());
    assertEquals(2.0, actual.getLatitude());
  }

  @Test
  void serialize() throws JacksonException {
    String expected = "{\"longitude\":1.0,\"latitude\":2.0}";
    SphericalLocation value = SphericalLocation.of(1.0, 2.0);
    String actual = mapper.writeValueAsString(value);
    assertEquals(expected, actual);
  }

  @Test
  void ofDoubles() {
    SphericalLocation actual = SphericalLocation.of(1.0, 2.0);
    assertEquals(1.0, actual.getLongitude());
    assertEquals(2.0, actual.getLatitude());
  }

  @Test
  void ofArray() {
    double[] array = new double[] {1.0, 2.0};
    SphericalLocation actual = SphericalLocation.of(array);
    assertEquals(1.0, actual.getLongitude());
    assertEquals(2.0, actual.getLatitude());
  }

  @Test
  void toArray() {
    double[] expected = new double[] {1.0, 2.0};
    double[] actual = SphericalLocation.of(1.0, 2.0).toArray();
    assertArrayEquals(expected, actual);
  }

  @Test
  void inside() {
    SphericalLocation nw = SphericalLocation.of(-16.5, -16.5);
    SphericalLocation so = SphericalLocation.of(+16.5, +16.5);
    BoundingBox<SphericalLocation> bounds = BoundingBox.of(nw, so);
    SphericalLocation value = SphericalLocation.of(+16.4, -16.4);
    assertTrue(value.inside(bounds));
  }
}
