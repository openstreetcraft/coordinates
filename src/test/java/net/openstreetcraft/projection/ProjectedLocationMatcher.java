package net.openstreetcraft.projection;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ProjectedLocationMatcher extends BaseMatcher<ProjectedLocation> {

  final ProjectedLocation expected;
  final double precision;
  
  @Override
  public boolean matches(Object item) {
    ProjectedLocation actual = (ProjectedLocation) item;
    assertEquals(expected.getX(), actual.getX(), precision);
    assertEquals(expected.getY(), actual.getY(), precision);
    return true;
  }

  @Override
  public void describeTo(Description description) {
    // TODO Auto-generated method stub
    
  }

}
