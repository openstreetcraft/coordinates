package net.openstreetcraft.projection;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.databind.ObjectMapper;

class ProjectedLocationTest {

  ObjectMapper mapper = new ObjectMapper();
  
  @Test
  void serialize() throws JacksonException {
    String expected = "{\"x\":-1.0,\"y\":2.0}";
    ProjectedLocation value = ProjectedLocation.of(-1.0, 2.0);
    String actual = mapper.writeValueAsString(value);
    assertEquals(expected, actual);
  }

  @Test
  void deserialize() throws JacksonException {
    String json = "{\"x\":-1.0,\"y\":2.0}";
    ProjectedLocation expected = ProjectedLocation.of(-1.0, 2.0);
    ProjectedLocation actual = mapper.readValue(json, ProjectedLocation.class);
    assertEquals(expected, actual);
  }

}
