package net.openstreetcraft.coordinates;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.MatcherAssert;

import lombok.RequiredArgsConstructor;
import net.openstreetcraft.projection.SphericalLocation;
import net.openstreetcraft.projection.SphericalLocationMatcher;

@RequiredArgsConstructor
public class BoundingBoxMatcher<T> extends BaseMatcher<BoundingBox<T>> {

  final BoundingBox<T> expected;
  final double precision;
  
  @Override
  public boolean matches(Object item) {
    @SuppressWarnings("unchecked")
    BoundingBox<T> actual = (BoundingBox<T>) item;
    if (actual.nw instanceof SphericalLocation) {
      SphericalLocation nw = (SphericalLocation) actual.nw;
      SphericalLocation so = (SphericalLocation) actual.so;
      MatcherAssert.assertThat(nw, new SphericalLocationMatcher((SphericalLocation) expected.nw, precision));
      MatcherAssert.assertThat(so, new SphericalLocationMatcher((SphericalLocation) expected.so, precision));
    } else if (actual.nw instanceof BlockLocation) {
      BlockLocation nw = (BlockLocation) actual.nw;
      BlockLocation so = (BlockLocation) actual.so;
      MatcherAssert.assertThat(nw, new BlockLocationMatcher((BlockLocation) expected.nw, precision));
      MatcherAssert.assertThat(so, new BlockLocationMatcher((BlockLocation) expected.so, precision));
    }
    return true;
  }

  @Override
  public void describeTo(Description description) {
    // TODO Auto-generated method stub
    
  }
}
