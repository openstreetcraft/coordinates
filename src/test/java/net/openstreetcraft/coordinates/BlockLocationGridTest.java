package net.openstreetcraft.coordinates;

import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;

class BlockLocationGridTest {

  @Test
  void apply() {
    BlockLocationGrid grid = new BlockLocationGrid(16.0);
    BoundingBox<BlockLocation> bounds = BoundingBox.of(
        BlockLocation.of(1.0, 20.0),
        BlockLocation.of(3.0, 40.0));
    BoundingBox<BlockLocation> expected = BoundingBox.of(
        BlockLocation.of(0.0, 16.0),
        BlockLocation.of(16.0, 48.0));

    MatcherAssert.assertThat(bounds.map(grid), new BoundingBoxMatcher(expected, 0.001));
  }

}
