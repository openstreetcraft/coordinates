package net.openstreetcraft.coordinates;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class BlockLocationMatcher extends BaseMatcher<BlockLocation> {

  final BlockLocation expected;
  final double precision;
  
  @Override
  public boolean matches(Object item) {
    BlockLocation actual = (BlockLocation) item;
    assertEquals(expected.x, actual.x, precision);
    assertEquals(expected.z, actual.z, precision);
    return true;
  }

  @Override
  public void describeTo(Description description) {
    // TODO Auto-generated method stub
    
  }

}
