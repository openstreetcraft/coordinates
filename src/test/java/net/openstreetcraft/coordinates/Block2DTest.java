package net.openstreetcraft.coordinates;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.Stream;

import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.databind.ObjectMapper;

class Block2DTest {

  ObjectMapper mapper = new ObjectMapper();

  @Test
  void deserialize() throws JacksonException {
    String json = "{\"x\":1,\"z\":2}";
    Block2D actual = mapper.readValue(json, Block2D.class);
    assertEquals(1, actual.getX());
    assertEquals(2, actual.getZ());
  }

  @Test
  void serialize() throws JacksonException {
    String expected = "{\"x\":1,\"z\":2}";
    Block2D value = Block2D.of(1, 2);
    String actual = mapper.writeValueAsString(value);
    assertEquals(expected, actual);
  }

  @Test
  void ofInts() {
    Block2D actual = Block2D.of(1, 2);
    assertEquals(1, actual.getX());
    assertEquals(2, actual.getZ());
  }

  @SuppressWarnings("unused")
  private static Stream<Arguments> provideBlockLocation() {
    return Stream.of(
        Arguments.of(Block2D.of(1, 2), BlockLocation.of(1.0, 2.0)),
        Arguments.of(Block2D.of(1, 2), BlockLocation.of(1.5, 2.5)),
        Arguments.of(Block2D.of(1, 2), BlockLocation.of(2.0, 3.0)),
        Arguments.of(Block2D.of(-1, -2), BlockLocation.of(-0.0, -1.0)),
        Arguments.of(Block2D.of(-1, -2), BlockLocation.of(-0.5, -1.5)),
        Arguments.of(Block2D.of(-1, -2), BlockLocation.of(-1.0, -2.0)));
  }

  @ParameterizedTest
  @MethodSource("provideBlockLocation")
  void ofBlockLocation(Block2D block, BlockLocation location) {
    Block2D actual = Block2D.of(BlockLocation.of(1.0, 2.0));
    assertEquals(1, actual.getX());
    assertEquals(2, actual.getZ());
  }

  @SuppressWarnings("unused")
  private static Stream<Arguments> provideContains() {
    return Stream.of(
        Arguments.of(Block2D.of(1, 2), BlockLocation.of(1.0, 2.0)),
        Arguments.of(Block2D.of(1, 2), BlockLocation.of(1.5, 2.5)),
        Arguments.of(Block2D.of(1, 2), BlockLocation.of(2.0, 3.0)),
        Arguments.of(Block2D.of(-1, -2), BlockLocation.of(-0.0, -1.0)),
        Arguments.of(Block2D.of(-1, -2), BlockLocation.of(-0.5, -1.5)),
        Arguments.of(Block2D.of(-1, -2), BlockLocation.of(-1.0, -2.0)));
  }

  @ParameterizedTest
  @MethodSource("provideContains")
  void contains(Block2D block, BlockLocation location) {
    assertTrue(block.contains(location));
  }

  @SuppressWarnings("unused")
  private static Stream<Arguments> provideContainsNot() {
    return Stream.of(
        Arguments.of(Block2D.of(1, 2), BlockLocation.of(0.9, 2.0)),
        Arguments.of(Block2D.of(1, 2), BlockLocation.of(2.0, 3.1)),
        Arguments.of(Block2D.of(-1, -2), BlockLocation.of( 0.1, -1.0)),
        Arguments.of(Block2D.of(-1, -2), BlockLocation.of(-1.0, -2.1)));
  }

  @ParameterizedTest
  @MethodSource("provideContainsNot")
  void containsNot(Block2D block, BlockLocation location) {
    assertFalse(block.contains(location));
  }

  @SuppressWarnings("unused")
  private static Stream<Arguments> provideBounds() {
    return Stream.of(
        Arguments.of(Block2D.of(1, 2), BoundingBox.of(
            BlockLocation.of(1.0, 2.0),
            BlockLocation.of(2.0, 3.0))),
        Arguments.of(Block2D.of(-1, -2), BoundingBox.of(
            BlockLocation.of(-1.0, -2.0),
            BlockLocation.of(-0.0, -1.0))));
  }

  @ParameterizedTest
  @MethodSource("provideBounds")
  void bounds(Block2D block, BoundingBox<BlockLocation> expected) {
    MatcherAssert.assertThat(block.bounds(), new BoundingBoxMatcher<>(expected, 0.0001));
  }

}
