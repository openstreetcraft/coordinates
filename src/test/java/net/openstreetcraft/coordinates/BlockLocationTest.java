package net.openstreetcraft.coordinates;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.databind.ObjectMapper;

class BlockLocationTest {

  ObjectMapper mapper = new ObjectMapper();

  @Test
  void deserialize() throws JacksonException {
    String json = "{\"x\":1,\"z\":2}";
    BlockLocation actual = mapper.readValue(json, BlockLocation.class);
    assertEquals(1.0, actual.getX());
    assertEquals(2.0, actual.getZ());
  }

  @Test
  void serialize() throws JacksonException {
    String expected = "{\"x\":1.0,\"z\":2.0}";
    BlockLocation value = BlockLocation.of(1.0, 2.0);
    String actual = mapper.writeValueAsString(value);
    assertEquals(expected, actual);
  }

  @Test
  void ofDoubles() {
    BlockLocation actual = BlockLocation.of(1, 2.0);
    assertEquals(1.0, actual.getX());
    assertEquals(2.0, actual.getZ());
  }

  @SuppressWarnings("unused")
  private static Stream<Arguments> provideInside() {
    return Stream.of(
        Arguments.of(BlockLocation.of(1.0, 2.0), BoundingBox.of(
            BlockLocation.of(1.0, 2.0),
            BlockLocation.of(2.0, 3.0))),
        Arguments.of(BlockLocation.of(2.0, 3.0), BoundingBox.of(
            BlockLocation.of(1.0, 2.0),
            BlockLocation.of(2.0, 3.0))),
        Arguments.of(BlockLocation.of(-1.0, -2.0), BoundingBox.of(
            BlockLocation.of(-1.0, -2.0),
            BlockLocation.of(-0.0, -1.0))));
  }

  @ParameterizedTest
  @MethodSource("provideInside")
  void inside(BlockLocation block, BoundingBox<BlockLocation> bounds) {
    assertTrue(block.inside(bounds));
  }

  @SuppressWarnings("unused")
  private static Stream<Arguments> provideOutside() {
    return Stream.of(
        Arguments.of(BlockLocation.of(0.9, 2.0), BoundingBox.of(
            BlockLocation.of(1.0, 2.0),
            BlockLocation.of(2.0, 3.0))),
        Arguments.of(BlockLocation.of(2.0, 3.1), BoundingBox.of(
            BlockLocation.of(1.0, 2.0),
            BlockLocation.of(2.0, 3.0))),
        Arguments.of(BlockLocation.of(-1.1, -2.0), BoundingBox.of(
            BlockLocation.of(-1.0, -2.0),
            BlockLocation.of(-0.0, -1.0))),
        Arguments.of(BlockLocation.of(-1.0, -0.9), BoundingBox.of(
            BlockLocation.of(-1.0, -2.0),
            BlockLocation.of(-0.0, -1.0))));
  }

  @ParameterizedTest
  @MethodSource("provideOutside")
  void outside(BlockLocation block, BoundingBox<BlockLocation> bounds) {
    assertFalse(block.inside(bounds));
  }
}
