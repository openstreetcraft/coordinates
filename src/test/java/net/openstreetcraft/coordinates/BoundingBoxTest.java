package net.openstreetcraft.coordinates;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.openstreetcraft.projection.ProjectedLocation;
import net.openstreetcraft.projection.Projection;
import net.openstreetcraft.projection.ProjectionBuilder;
import net.openstreetcraft.projection.SphericalLocation;

class BoundingBoxTest {
  
  ObjectMapper mapper = new ObjectMapper();
  
  @Test
  void serialize() throws JacksonException {
    String expected = "{\"nw\":{\"x\":-16.0,\"z\":32.0},\"so\":{\"x\":0.0,\"z\":48.0}}";
    ChunkLocation location = ChunkLocation.of(-1.0, 2.0);
    String actual = mapper.writeValueAsString(location.bounds());
    assertEquals(expected, actual);
  }
  
  @Test
  void forEach() {
    Projection projection = new ProjectionBuilder("earth").build();
    
    BoundingBox<SphericalLocation> expected = BoundingBox.of(
        SphericalLocation.of(0.0, 30.0),
        SphericalLocation.of(60.0, 90.0));
    
    BoundingBox<SphericalLocation> actual = BoundingBox.of(
        ProjectedLocation.of(0.0, 3335847.7993367617),
        ProjectedLocation.of(6671695.598673523, 10007543.398010286))
            .forEach(projection::transform);
    
    MatcherAssert.assertThat(actual, new BoundingBoxMatcher<>(expected, 0.5));
  }
  
}
