package net.openstreetcraft.coordinates;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.Stream;

import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.databind.ObjectMapper;

class ChunkLocationTest {

  ObjectMapper mapper = new ObjectMapper();

  @Test
  void deserialize() throws JacksonException {
    String json = "{\"x\":1,\"z\":2}";
    ChunkLocation actual = mapper.readValue(json, ChunkLocation.class);
    assertEquals(1.0, actual.getX());
    assertEquals(2.0, actual.getZ());
  }

  @Test
  void serialize() throws JacksonException {
    String expected = "{\"x\":1.0,\"z\":2.0}";
    ChunkLocation value = ChunkLocation.of(1.0, 2.0);
    String actual = mapper.writeValueAsString(value);
    assertEquals(expected, actual);
  }

  @Test
  void ofDoubles() {
    ChunkLocation actual = ChunkLocation.of(1.0, 2.0);
    assertEquals(1.0, actual.getX());
    assertEquals(2.0, actual.getZ());
  }

  @Test
  void ofChunk2D() {
    ChunkLocation actual = ChunkLocation.of(Chunk2D.of(1, 2));
    assertEquals(1.0, actual.getX());
    assertEquals(2.0, actual.getZ());
  }

  @SuppressWarnings("unused")
  private static Stream<Arguments> provideBounds() {
    return Stream.of(
        Arguments.of(ChunkLocation.of(1.0, 2.0), BoundingBox.of(
            BlockLocation.of(16.0, 32.0),
            BlockLocation.of(32.0, 48.0))),
        Arguments.of(ChunkLocation.of(1.5, 2.5), BoundingBox.of(
            BlockLocation.of(24.0, 40.0),
            BlockLocation.of(40.0, 56.0))),
        Arguments.of(ChunkLocation.of(-1.0, -2.0), BoundingBox.of(
            BlockLocation.of(-16.0, -32.0),
            BlockLocation.of(0.0, -16.0))));
  }

  @ParameterizedTest
  @MethodSource("provideBounds")
  void bounds(ChunkLocation location, BoundingBox<BlockLocation> expected) {
    MatcherAssert.assertThat(location.bounds(), new BoundingBoxMatcher<>(expected, 0.0001));
  }
}
