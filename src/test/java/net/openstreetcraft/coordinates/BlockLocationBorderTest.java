package net.openstreetcraft.coordinates;

import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;

class BlockLocationBorderTest {

  @Test
  void apply() {
    BlockLocationBorder border = new BlockLocationBorder(0.5);
    BoundingBox<BlockLocation> bounds = BoundingBox.of(
        BlockLocation.of(1.0, 2.0),
        BlockLocation.of(3.0, 4.0));
    BoundingBox<BlockLocation> expected = BoundingBox.of(
        BlockLocation.of(0.5, 1.5),
        BlockLocation.of(3.5, 4.5));

    MatcherAssert.assertThat(bounds.map(border), new BoundingBoxMatcher(expected, 0.001));
  }

}
