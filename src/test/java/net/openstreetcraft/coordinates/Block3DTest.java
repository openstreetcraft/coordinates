package net.openstreetcraft.coordinates;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.databind.ObjectMapper;

class Block3DTest {

  ObjectMapper mapper = new ObjectMapper();

  @Test
  void deserialize() throws JacksonException {
    String json = "{\"x\":1,\"y\":2,\"z\":3}";
    Block3D actual = mapper.readValue(json, Block3D.class);
    assertEquals(1, actual.getX());
    assertEquals(2, actual.getY());
    assertEquals(3, actual.getZ());
  }

  @Test
  void serialize() throws JacksonException {
    String expected = "{\"x\":1,\"z\":3,\"y\":2}";
    Block3D value = Block3D.of(1, 2, 3);
    String actual = mapper.writeValueAsString(value);
    assertEquals(expected, actual);
  }

  @Test
  void ofInts() {
    Block3D actual = Block3D.of(1, 2, 3);
    assertEquals(1, actual.getX());
    assertEquals(2, actual.getY());
    assertEquals(3, actual.getZ());
  }

}
